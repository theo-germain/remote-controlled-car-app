[![pipeline status](https://gitlab.com/theo.germain/remote-controlled-car-app/badges/master/pipeline.svg)](https://gitlab.com/theo.germain/remote-controlled-car-app/-/commits/master)
[![coverage report](https://gitlab.com/theo.germain/remote-controlled-car-app/badges/master/coverage.svg)](https://gitlab.com/theo.germain/remote-controlled-car-app/-/commits/master)

# remote_controlled_car

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
