import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_datasource.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_repository_impl.dart';

class MockFlashLedDatasource extends Mock implements FlashLedDatasource {}

void main() {
  FlashLedRepositoryImpl repository;
  MockFlashLedDatasource mockDatasource;

  setUp(() {
    mockDatasource = MockFlashLedDatasource();
    repository = FlashLedRepositoryImpl(datasource: mockDatasource);
  });

  test(
      'should call the sendFlashLedSTateUpdate datasource method with the right boolean',
      () {
    // Given
    final bool isLedOn = true;

    // When
    repository.setCarLedState(isLedOn);

    // Then
    verify(mockDatasource.sendFlashLedStateUpdate(isLedOn));
  });
}
