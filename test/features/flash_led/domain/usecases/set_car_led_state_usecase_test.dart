import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/features/flash_led/domain/repositories/flash_led_repository.dart';
import 'package:remote_controlled_car/features/flash_led/domain/usecases/set_car_led_state_usecase.dart';

class MockFlashLedRepository extends Mock implements FlashLedRepository {}

void main() {
  SetCarLedStateUsecase usecase;
  MockFlashLedRepository mockRepository;

  setUp(() {
    mockRepository = MockFlashLedRepository();
    usecase = SetCarLedStateUsecase(repository: mockRepository);
  });

  test(
      'should call the setCarLedState repository method with the right boolean',
      () {
    // Given
    final bool isLedOn = true;

    // When
    usecase(isLedOn);

    // Then
    verify(mockRepository.setCarLedState(isLedOn));
  });
}
