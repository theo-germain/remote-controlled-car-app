import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';

const String SERVICE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b";
const String CAM_CHARACTERISTIC_UUID = "db27cb60-cdcf-4075-9be7-fef1621f34d7";
const String CTRL_CHARACTERISTIC_UUID = "07725e51-32f1-4303-adc7-9e4a4239048b";

class BluetoothConnection extends Connection {
  final FlutterBlue flutterBlue;
  final String serverName;

  BluetoothDevice _server;
  final _imgBuilder = BytesBuilder(copy: false);
  int _imgSize;

  final StreamController<Uint8List> _bluetoothStream =
      StreamController<Uint8List>();
  final StreamController<Uint8List> _bluetoothSink =
      StreamController<Uint8List>();

  BluetoothConnection({@required this.flutterBlue, @required this.serverName})
      : assert(flutterBlue != null),
        assert(serverName != null) {
    initClient();
  }

  @override
  Sink get sink => _bluetoothSink.sink;

  @override
  Stream get stream => _bluetoothStream.stream;

  Future<void> connect() async {
    print('CONNECT');
    print('*** Location must be turned on ***');
    var scanDone = flutterBlue.startScan();
    flutterBlue.scanResults.listen((List<ScanResult> results) {
      _server = results
          .firstWhere((element) => element.device.name == serverName,
              orElse: () => null)
          ?.device;
      if (_server != null) flutterBlue.stopScan();
    });
    await scanDone;
    await _server.connect();
    print('CONNECTED');
  }

  Future<void> changeMtuSize() async {
    //final mtu = await _server.mtu.first;
    await _server.requestMtu(512);
    print('MTU SIZE CHANGED');
    await Future.delayed(Duration(seconds: 1));
  }

  Future<void> initClient() async {
    List<BluetoothDevice> connectedDevices = await flutterBlue.connectedDevices;
    _server = connectedDevices.firstWhere((device) => device.name == serverName,
        orElse: () => null);

    if (_server == null) await connect();

    await changeMtuSize();

    List<BluetoothService> services = await _server.discoverServices();
    final BluetoothService service =
        services.firstWhere((service) => service.uuid == Guid(SERVICE_UUID));
    final BluetoothCharacteristic characteristic = service.characteristics
        .firstWhere((characteristic) =>
            characteristic.uuid == Guid(CAM_CHARACTERISTIC_UUID));

    await characteristic.setNotifyValue(true);

    characteristic.value.listen((event) {
      //print('NEW EVENT');
      if (_imgSize == null) {
        try {
          var jsonDecoded = json.decode(String.fromCharCodes(event));
          _imgSize = jsonDecoded['size'];
        } on FormatException {
          _imgSize = null;
        }
      } else if (_imgSize != null) {
        //print('BUILDING IMAGE');
        _imgBuilder.add(event);
        //print('${_imgBuilder.length}/$_imgSize');
        if (_imgBuilder.length >= _imgSize) {
          //print('IMAGE DONE');
          _bluetoothStream.sink.add(_imgBuilder.takeBytes());
          _imgSize = null;
        }
      }
    });

    final BluetoothCharacteristic ctrlCharacteristic = service.characteristics
        .firstWhere((characteristic) =>
            characteristic.uuid == Guid(CTRL_CHARACTERISTIC_UUID));

    _bluetoothSink.stream.listen((event) async {
      bool msgSent = false;
      while (msgSent == false) {
        try {
          await ctrlCharacteristic.write(event, withoutResponse: true);
          msgSent = true;
        } catch (e) {
          print(e);
        }
      }
    });
  }

  void dispose() {
    _bluetoothStream.close();
    _bluetoothSink.close();
    _server.disconnect();
  }
}
