import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';

abstract class CarControlRepository {
  void sendCarDirection(Stream<CarControl> stream);
}
