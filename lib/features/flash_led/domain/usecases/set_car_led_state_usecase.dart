import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/flash_led/domain/repositories/flash_led_repository.dart';

class SetCarLedStateUsecase {
  final FlashLedRepository repository;

  SetCarLedStateUsecase({@required this.repository})
      : assert(repository != null);

  void call(bool isLedOn) => repository.setCarLedState(isLedOn);
}
