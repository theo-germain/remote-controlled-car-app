import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/camera_stream/data/datasources/camera_stream_datasource_impl.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_datasource.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_repository_impl.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/repositories/camera_stream_repository.dart';

GetIt locator = GetIt.instance;

void cameraStreamDataSetup() {
  // repositories
  locator.registerLazySingleton<CameraStreamRepository>(
      () => CameraStreamRepositoryImpl(datasource: locator()));

  // datasources
  locator.registerLazySingleton<CameraStreamDatasource>(
      () => CameraStreamDatasourceImpl(connection: locator()));
}
