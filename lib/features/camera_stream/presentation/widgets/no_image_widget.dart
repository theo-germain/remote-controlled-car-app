import 'package:flutter/material.dart';

class NoImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      child: Center(
        child: Icon(
          Icons.videocam_off_outlined,
          size: 80,
        ),
      ),
    );
  }
}
