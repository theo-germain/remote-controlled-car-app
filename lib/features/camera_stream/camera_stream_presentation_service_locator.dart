import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/view_models/camera_stream_view_model.dart';

GetIt locator = GetIt.instance;

void cameraStreamPresentationSetup() {
  // viewModel
  locator.registerLazySingleton(() => CameraStreamViewModel(
      getCameraStreamUsecase: locator(), imageClassifierViewModel: locator()));
}
